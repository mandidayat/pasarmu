package com.mobiledevelopt.freshfastfood;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.mobiledevelopt.freshfastfood.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
