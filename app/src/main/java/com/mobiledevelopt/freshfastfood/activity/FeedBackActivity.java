package com.mobiledevelopt.freshfastfood.activity;

import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.AppBarLayout;
import com.mobiledevelopt.freshfastfood.model.User;
import com.mobiledevelopt.freshfastfood.R;
import com.mobiledevelopt.freshfastfood.utils.CustPrograssbar;
import com.mobiledevelopt.freshfastfood.utils.SessionManager;
import com.mobiledevelopt.freshfastfood.retrofit.APIClient;
import com.mobiledevelopt.freshfastfood.retrofit.GetResult;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class FeedBackActivity extends AppCompatActivity implements GetResult.MyListener {

    @BindView(R.id.ed_feedback)
    TextInputEditText edFeedback;
    @BindView(R.id.btn_submit)
    TextView btnSubmit;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    User user;
    SessionManager sessionManager;
    CustPrograssbar custPrograssbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        sessionManager = new SessionManager(FeedBackActivity.this);
        user = sessionManager.getUserDetails("");
        custPrograssbar = new CustPrograssbar();
    }
    private void UserFeedBack() {
        custPrograssbar.PrograssCreate();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("msg", edFeedback.getText().toString());
            jsonObject.put("rate", String.valueOf(ratingBar.getRating()));
            jsonObject.put("uid", user.getId());
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().SendFeed((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "1");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void callback(JsonObject result, String callNo) {
        custPrograssbar.ClosePrograssBar();
        try {
            if (result != null) {

                    JSONObject object = new JSONObject(result.toString());
                    Toast.makeText(FeedBackActivity.this, "" + object.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                    if (object.getString("Result").equals("true")) {
                        ratingBar.setRating(0.0f);
                        edFeedback.setText("");
                    }

            }
        } catch (Exception e) {
            e.toString();
        }
    }
    @OnClick(R.id.btn_submit)
    public void onViewClicked() {
        if (validation()) {
            UserFeedBack();
        }
    }
    private boolean validation() {
        if (edFeedback.getText().toString().isEmpty()) {
            edFeedback.setError("Masukkan Tanggapan");
            return false;
        }
        if (String.valueOf(ratingBar.getRating()).equals("0.0")) {
            Toast.makeText(FeedBackActivity.this, "Berikan Rating", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
